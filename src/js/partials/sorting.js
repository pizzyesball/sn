if ($('.js-sort').length) {
    $('.js-sort').on('click', function () {
	    $(this).toggleClass('is-expand').next().slideToggle(500);
    });

    $('.js-sort-link').on('click', function () {

    	var $checkbox = $(this).children('input');

        if ($checkbox.is(':checked')) {
        	$('.js-sort-link').parent().find('input').removeAttr('checked');
            $checkbox.attr('checked', 'checked');
        }

	    if ($(this).hasClass('active')) {

	    } else {
	        $(this).addClass('active');
	        $('.js-sort-link').not(this).removeClass('active');
	        $('.js-sort').removeClass('is-expand').next().delay(500).slideUp(500);
	    }
    });
}
