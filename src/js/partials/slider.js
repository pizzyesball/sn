function setHeiHeight() {
    $('.js-full').css({
        height: $(window).height() + 'px'
    });
}

setHeiHeight();

$(window).resize(setHeiHeight);


$('.js-head-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    startProgressbar();
}).slick({
    slidesToShow: 1,
  	slidesToScroll: 1,
    arrows: false,
    dots: true,
    customPaging: function (slider, i) {
        var thumb = jQuery(slider.$slides[i]).data();
        return '<svg class="progress" viewBox="0 0 42 42"><circle class="progress-bar" r="19" cx="21" cy="21"/></svg><a>'+('0'+(i+1)).slice(-2)+'</a>';
    }
}).on('afterChange', function (event, slick, currentSlide, nextSlide) {
    var $slideColor = $('.slick-slide.slick-current').data('color');
    $(this).parent().attr('data-color', $slideColor);
});

var time = 3;
var tick, percentTime = 0;
var $rbar = $('.progress-bar');
var rlen = 2 * Math.PI * $rbar.attr('r');

function startProgressbar() {
    percentTime = 0;
    clearTimeout(tick);
    tick = setInterval(interval, 20);
}

function interval() {

    percentTime += 1 / (time + 0.1);
    $rbar.css({
        strokeDasharray: rlen,
        strokeDashoffset: rlen * (1 - percentTime / 100)
    });
    if (percentTime >= 100) {
    	$('.js-head-slider').slick('slickNext');
        percentTime = 0;
        startProgressbar();
    }
}

startProgressbar();
