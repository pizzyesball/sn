var windowsOS = (navigator.userAgent.toLowerCase().indexOf('windows') !== -1);
var $body = $('body');

var scrollWidth = function scrollbarWidth() {
    var block = $('<div>').css({'height': '50px','width': '50px'}),
        indicator = $('<div>').css({'height': '200px'});

    $body.append(block.append(indicator));
    var w1 = $('div', block).innerWidth();
    block.css('overflow-y', 'scroll');
    var w2 = $('div', block).innerWidth();
    $(block).remove();
    return (w1 - w2);
};

$('.js-catalog-burger').css('margin-right', scrollWidth);

$('.js-burger').on('click', function () {
    $(this).siblings().css('display', 'flex');
    $(this).siblings().delay(50).queue(function (next) {
        $(this).addClass('is-expand');
        next();
    });

    $('.js-catalog-burger').css('margin-right', '0px');
    $body.addClass('no-scroll');
});

$('.js-burger-close').on('click', function () {
    $(this).parent().removeClass('is-expand').delay(500).queue(function (next) {
        $(this).hide();
        next();
    });

    $body.removeClass('no-scroll windows');
    $('.js-catalog-burger').css('margin-right', scrollWidth);
});

$('.js-burger-link').on('click', function (event) {

    if ($(window).width() >= '768') {
        event.preventDefault();
    }

    if ($(this).hasClass('is-expand')) {

    } else {
        $(this).addClass('is-expand').next().addClass('is-expand');
        $('.js-burger-link').not(this).removeClass('is-expand').next().removeClass('is-expand');
    }
});

$('.js-burger-link').hover(function () {
    $(this).trigger('click');
});

$('.js-catalog-burger').on('click', function () {

    $body.addClass('no-scroll');
    $('.b-catalog-menu__inner').show();
    $(this).parent().delay(50).queue(function (next) {
        $(this).addClass('is-expand').css({'position': 'fixed', 'z-index': '10'});
        next();
    });

});

$('.js-catalog-burger-close').on('click', function () {

    $('.js-catalog-burger').parent().removeClass('is-expand').delay(400).queue(function (next) {
        $('.b-catalog-menu__inner').hide();
        $(this).css({'position': '', 'z-index': ''});
        next();
    });
    $body.removeClass('no-scroll windows');

});

$(window).scroll(function () {
    var height = $(window).scrollTop();
    var burger = $('.js-burger').parent();
    var burgerPosition = burger.position();
    var burgerHeight = burger.height();
    var heightToggle = $('.js-head').height() - burgerPosition.top - burgerHeight;

    if (height > heightToggle) {
        $('.js-burger').parent().addClass('fill');
    } else {
        $('.js-burger').parent().removeClass('fill');
    }
});
