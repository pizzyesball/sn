if ($('.js-nav').length) {

    $('.js-nav').on('click', function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 700);
    });
}

if ($('.js-nav-check').length) {
    $('.js-nav-check').on('click', function () {

        var $checkbox = $(this).children('input');

        if ($checkbox.is(':checked')) {
            $('.js-nav-check').parent().find('input').removeAttr('checked');
            $checkbox.attr('checked', 'checked');
        }

        if ($(this).hasClass('active')) {

        } else {
            $(this).addClass('active');
            $('.js-nav-check').not(this).removeClass('active');
        }
    });
}
