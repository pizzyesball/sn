$('.js-genesis-slider').each(function () {

    var $slider = $(this);

    $slider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        infinite: true,
        variableWidth: true,
        centerMode: true,
        nextArrow: $slider.siblings().children('.js-next'),
        prevArrow: $slider.siblings().children('.js-prev')
    });

    $slider.on('afterChange', function (event, slick, currentSlide) {
        $slider.siblings().find('span.active').text(currentSlide + 1);
    });
});
