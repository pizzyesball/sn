$(document).ready(function () {
    var SETTINGS = {
        ignore: '.ignore, [type="hidden"]',
    };
    $.validator.setDefaults({debug: true});
    $.extend($.validator.messages, {
        required: 'Это поле необходимо заполнить.',
        remote: 'Пожалуйста, введите правильное значение.',
        email: 'Укажите электронный адрес',
        url: 'Пожалуйста, введите корректный URL.',
        date: 'Пожалуйста, введите корректную дату.',
        dateISO: 'Пожалуйста, введите корректную дату в формате ISO.',
        number: 'Пожалуйста, введите число.',
        digits: 'Пожалуйста, вводите только цифры.',
        creditcard: 'Пожалуйста, введите правильный номер кредитной карты.',
        equalTo: 'Пожалуйста, введите такое же значение ещё раз.',
        extension: 'Пожалуйста, выберите файл с правильным расширением.',
        maxlength: $.validator.format('Пожалуйста, введите не больше {0} символов.'),
        minlength: $.validator.format('Пожалуйста, введите не меньше {0} символов.'),
        rangelength: $.validator.format('Пожалуйста, введите значение длиной от {0} до {1} символов.'),
        range: $.validator.format('Пожалуйста, введите число от {0} до {1}.'),
        max: $.validator.format('Пожалуйста, введите число, меньшее или равное {0}.'),
        min: $.validator.format('Пожалуйста, введите число, большее или равное {0}.')
    });


    $.validator.addMethod('email', function (value, element) {
        var regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if ($(element).hasClass('js-email')) {
            if ($(element).prop('required') || value.replace(/\s/g, '').length) {
                return regEmail.test(value);
            } else {
                return true;
            }
        } else {
            return true;
        }
    }, $.validator.messages.email);

    $('body').on('initValidation', function () {
        $('.js-form-validate').each(function () {
            var $this = $(this);

            var localSettings = {};

            if ($this.attr('name')=='contacts'){
                localSettings.errorPlacement = function(error, element) {
                    element.attr('placeholder', error.text());
                }
            }

            var settings = $.extend(localSettings, SETTINGS);

            var validator = $this.validate(settings);

            $this.on('reset', function (event) {
                validator.resetForm();
            });

            $this.on('submit', function (event) {
                if (validator.numberOfInvalids() > 0) {
                    event.stopImmediatePropagation();
                } else {
                    var $form = $this;
                    var url = $form.attr('action') || location.href;
                    var method = $form.attr('method');
                    var event = $form.data('event');
                    $.ajax({
                        url: url,
                        method: method,
                        data: $form.serializeArray(),
                        dataType: 'json',
                        success: function (data) {
                            if (event && data){
                                $('body').trigger(event, data);
                            }
                        }
                    });
                }
            });

            $this.find('.js-check-form').on('click', function (event) {
                let $that = $(this);
                if (validator.numberOfInvalids()) {
                    return;
                }
                validator.form();

                setTimeout(function () {
                    if (validator.numberOfInvalids() == 0) {
                        $that.trigger('popup.onOpen');
                    }
                }, 1000)
            });
        });
    });

    $('body').trigger('initValidation');
});
