$(document).ready(function () {
    var $banner = $('.js-move');
    var FRONT_SPEED = 0.015;
    var MID_SPEED = 0.009;
    var BACK_SPEED = 0.003;

    function moving() {
        var $container = $(this);
        var $front = $container.find('.js-move-image.front');
        var $mid = $container.find('.js-move-image.mid');
        var $back = $container.find('.js-move-image.back');
        $container.on('mousemove', function (event) {
            var x = event.pageX;
            var y = event.pageY;
            $front.css('transform', 'translate(' + x * FRONT_SPEED + 'px , ' + y * FRONT_SPEED + 'px)');
            $mid.css('transform', 'translate(' + x * MID_SPEED + 'px , ' + y * MID_SPEED + 'px)');
            $back.css('transform', 'translate(' + x * BACK_SPEED + 'px , ' + y * BACK_SPEED + 'px)');
        });
    }
    // $('.js-move-image').fadeIn(900);
    $banner.each(moving);

    // $('header').on('mousemove', function (event) {
    //     $('.slick-current').trigger(event);
    // });
});
