// // // if NOT requireJS
$(document).ready(function () {
    $('.js-fabric').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 400,
        mobileFirst: true,
        nextArrow: $('.js-fabric').parent().find('.js-next'),
        prevArrow: $('.js-fabric').parent().find('.js-prev')
    });

    $('.js-fabric').on('afterChange', function (event, slick, currentSlide) {
        $('.js-fabric').siblings().find('span.active').text(currentSlide + 1);
    });
});
