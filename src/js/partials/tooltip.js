$('.js-tooltip').tooltipster({
    arrow: false,
    distance: 0,
    side: 'top',
    trigger: ('ontouchstart' in window) ? 'click' : 'hover'
});
