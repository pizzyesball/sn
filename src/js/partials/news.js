// // // if NOT requireJS
$(document).ready(function () {
    $('.js-news').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        variableWidth: true,
        speed: 400,
        mobileFirst: true,
        nextArrow: $('.js-news').parent().find('.js-next'),
        prevArrow: $('.js-news').parent().find('.js-prev'),
        responsive: [
            {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 2,
                    centerMode: false,
                    variableWidth: false,
                }
            },
            {
                breakpoint: 1439,
                settings: {
                    slidesToShow: 3,
                    centerMode: true,
                    variableWidth: true,
                }
            }
        ]
    });
});
