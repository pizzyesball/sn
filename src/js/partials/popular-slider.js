$('.js-popular-slider').each(function () {

    var $slider = $(this);

    $slider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        infinite: true,
        mobileFirst: true,
        nextArrow: $slider.siblings().children('.next'),
        prevArrow: $slider.siblings().children('.prev'),
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 3
                }
            },{
                breakpoint: 1023,
                settings: {
                    slidesToShow: 4
                }
            },{
                breakpoint: 1365,
                settings: {
                    slidesToShow: 5
                }
            }
        ]
    });
});
