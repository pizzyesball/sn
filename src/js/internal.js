(function() {

  function documentReady() {
    @@include('partials/utilites/mediaHandler.js');
    @@include('partials/utilites/ClassEvent.js');
    @@include('partials/utilites/defineSetter.js');
    @@include('partials/utilites/AnimationQueue.js');
    @@include('partials/utilites/onscrollAnimate.js');
    @@include('partials/news.js');
    @@include('partials/fabric.js');
    @@include('partials/popup.js');
    @@include('partials/ie-detect.js');

    @@include('partials/js-burger.js');
    @@include('partials/slider.js');
    @@include('partials/scroll.js');
    @@include('partials/parallax-scroll.js');
    @@include('partials/parallax.js');
    @@include('partials/sorting.js');
    @@include('partials/filter.js');
    @@include('partials/image-slider.js');
    @@include('partials/popular-slider.js');
    @@include('partials/genesis-slider.js');
    @@include('partials/tooltip.js');
    @@include('partials/yamaps.js');
    @@include('partials/nav.js');
    @@include('partials/inputmask.js');
    @@include('partials/validation.js');
  };

  document.addEventListener("DOMContentLoaded", documentReady);

})();